# mybatis-learn

#### 介绍
学习mybatis的案例， 学习资料均来源于[官方文档](http://www.mybatis.org/mybatis-3/zh/index.html)

src/main/java为对应源码，
src/test/java为测试例子， 

src/main/resources包下的user.sql、email.sql为mysql脚本， 是本案例的测试数据。