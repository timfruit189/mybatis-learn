/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-07-03 17:35:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for email
-- ----------------------------
DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `email_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '映射user的user_id',
  `email_address` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of email
-- ----------------------------
INSERT INTO `email` VALUES ('1', '1', 'xxxxxx@qq.com', 'xxxxxx');
INSERT INTO `email` VALUES ('2', '1', 'yyyyyy@189.cn', 'yyyyyy');
INSERT INTO `email` VALUES ('3', '1', 'zzzzzz@163.com', 'zzzzzz');
INSERT INTO `email` VALUES ('4', '2', 'ddddd@qq.com', 'ddddd');
INSERT INTO `email` VALUES ('5', '2', 'eeeeee@qq.com', 'eeeeee');
