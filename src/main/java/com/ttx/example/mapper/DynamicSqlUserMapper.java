package com.ttx.example.mapper;

import com.ttx.example.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TimFruit
 * @date 2019/7/4 8:18
 */

public interface DynamicSqlUserMapper {
    
    User selectUserByUserNameAndAge(@Param("userName") String userName, @Param("age") Integer age);
    
    List<User> selectUserLikeByChoose(@Param("userName") String userName, @Param("age") Integer age, @Param("country") String country);

    List<User> selectUserLikeByWhere(@Param("userName") String userName,  @Param("country") String country);

    List<User> selectUserLikeByTrim(@Param("userName") String userName,  @Param("country") String country);

    List<User> selectUserByUserIdsByForeach(@Param("userIds") List<Integer> userIds);
}
