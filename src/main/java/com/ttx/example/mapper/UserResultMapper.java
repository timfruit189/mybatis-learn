package com.ttx.example.mapper;

import com.ttx.example.pojo.UserResult;
import com.ttx.example.pojo.UserResultEmail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TimFruit
 * @date 2019/7/3 15:31
 */

public interface UserResultMapper {
    UserResultEmail selectUserAndEmailByUserId(@Param("userId") int userId);
    
    List<UserResult> selectUserByUserNameLike(@Param("userName") String userName);
}
