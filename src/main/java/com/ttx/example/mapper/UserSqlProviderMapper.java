package com.ttx.example.mapper;

import com.ttx.example.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.SelectProvider;

/**
 * @author TimFruit
 * @date 2019/7/5 14:23
 */

public interface UserSqlProviderMapper {
    
    @ResultMap("UserMap")
    @SelectProvider(type = UserSqlProvider.class, method = "buildSelectUserById")
    User selectUserById(@Param("userId") int userId);
    
}

