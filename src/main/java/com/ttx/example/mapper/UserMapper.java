package com.ttx.example.mapper;

import com.ttx.example.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    
    User selectUser(@Param("userId") int userId);
    
    void insertUser(@Param("user") User user);
    
    void updateUserNameById(@Param("userId") int userId, @Param("userName") String userName);
    
    void deleteUserById(@Param("userId") int userId);
}
