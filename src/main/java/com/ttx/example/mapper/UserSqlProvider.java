package com.ttx.example.mapper;

import org.apache.ibatis.jdbc.SQL;

/**
 * @author TimFruit
 * @date 2019/7/5 14:24
 */

public class UserSqlProvider {
    
    public static String buildSelectUserById(){
        return new SQL(){{
            SELECT("*");
            FROM("user");
            WHERE("user_id = #{userId}");
            ORDER_BY("user_id");
        }}.toString();
    }
    
}
