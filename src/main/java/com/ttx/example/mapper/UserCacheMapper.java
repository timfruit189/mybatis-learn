package com.ttx.example.mapper;

import com.ttx.example.entity.User;
import com.ttx.example.entity.UserCache;
import org.apache.ibatis.annotations.Param;

/**
 * 测试使用mybatis的缓存机制 
 *  官方文档 http://www.mybatis.org/mybatis-3/zh/sqlmap-xml.html#cache
 */
public interface UserCacheMapper {
    
    UserCache selectUserCacheByUserId(@Param("userId") int userId);

    User selectUserByUserId(@Param("userId") int userId);
}
