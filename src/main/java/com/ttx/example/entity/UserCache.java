package com.ttx.example.entity;

import java.io.Serializable;

/**
 * @author TimFruit
 * @date 2019/7/1 15:52
 */
// 为使用二级缓存 <cache readOnly="false" />， 需要实现Serializable接口 ，当readOnly为"true"时则不需要 
public class UserCache  implements Serializable{
    private Integer userId;
    private String userName;
    private Integer age;
    private String country;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
