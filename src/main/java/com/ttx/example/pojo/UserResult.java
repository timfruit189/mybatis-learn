package com.ttx.example.pojo;

/**
 * 用于学习结果映射
 * @author TimFruit
 * @date 2019/7/1 15:52
 */

public class UserResult {
    private Integer userId;
    private String userName;
    private Integer age;
    private String country;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
