package com.ttx.example.pojo;

import java.util.List;

/**
 * @author TimFruit
 * @date 2019/7/3 15:51
 */

public class UserResultEmail {
    
    private Integer userId;  //需要使用userId标识
    private UserResult userResult;
    private List<Email> emailList;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserResult getUserResult() {
        return userResult;
    }

    public void setUserResult(UserResult userResult) {
        this.userResult = userResult;
    }

    public List<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }
}
