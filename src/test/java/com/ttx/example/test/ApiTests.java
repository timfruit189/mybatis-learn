package com.ttx.example.test;

import com.ttx.example.entity.User;
import com.ttx.example.mapper.DynamicSqlUserMapper;
import com.ttx.example.mapper.UserMapper;
import com.ttx.example.mapper.UserResultMapper;
import com.ttx.example.mapper.UserSqlProviderMapper;
import com.ttx.example.pojo.Email;
import com.ttx.example.pojo.UserResult;
import com.ttx.example.pojo.UserResultEmail;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.logging.log4j.Log4jImpl;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.*;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

/**
 * 学习mybatis sqlsession api 
 *   官网网址： http://www.mybatis.org/mybatis-3/zh/java-api.html#sqlSessions
 * @author TimFruit
 * @date 2019/7/4 14:24
 */

public class ApiTests {
    private SqlSessionFactory sqlSessionFactory;
    
    
    @Before
    public void initWithXmlConfig() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
    
    
    public void initWithCodeConfig() throws IOException {
        
        //这是使用代码构建SqlSessionFactory的过程
        //根据xml配置文件的构建方法可以参考MybatisTests#init()方法
        
        
        Properties properties=Resources.getResourceAsProperties("datasource.properties");

        String driver=properties.getProperty("driver").trim();
        String url=properties.getProperty("url").trim();
        String username=properties.getProperty("username").trim();
        String password=properties.getProperty("password").trim();
        DataSource dataSource = new PooledDataSource(driver, url, username, password);
        
        TransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment environment = new Environment("development", transactionFactory, dataSource);

        Configuration configuration = new Configuration(environment);
        configuration.setLazyLoadingEnabled(true);
//        configuration.setEnhancementEnabled(true);
        
        
        
        // 通过注册类， 来配置 
        configuration.getTypeAliasRegistry().registerAlias(User.class);
        configuration.getTypeAliasRegistry().registerAlias(Email.class);
        configuration.getTypeAliasRegistry().registerAlias(UserResult.class);
        configuration.getTypeAliasRegistry().registerAlias(UserResultEmail.class);
        
        configuration.addMapper(UserResultMapper.class);
        configuration.addMapper(UserMapper.class);
        configuration.addMapper(DynamicSqlUserMapper.class);
        // MapperRegistry#addMapper() 
        // 该方法会先加载解析mapper.xml文件， 后解析mapper.class中的注解
        
        //日志
        //fixme 打印sql日志
        configuration.setLogImpl(Log4jImpl.class);
        
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        this.sqlSessionFactory = builder.build(configuration);

    }
    
    
    @Test
    public void test(){

        try(SqlSession sqlSession=sqlSessionFactory.openSession();){ // 使用try() , 是为了关闭资源

            UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
            User user=userMapper.selectUser(1);
            sqlSession.commit();

            System.out.println("name: "+user.getUserName()+", age: "+user.getAge());

            Assert.assertEquals(new Integer(1), user.getUserId());
            
        }
        
    }
    
    
    /*
    使用sqlSessionFactory创建sqlSession实例时，需要考虑3个问题：
    
    1. 事务处理：我需要在 session 使用事务或者使用自动提交功能（auto-commit）吗？（通常意味着很多数据库和/或 JDBC 驱动没有事务）
   
    2. 连接：我需要依赖 MyBatis 获得来自数据源的配置吗？还是使用自己提供的配置？
       
    3. 执行语句：我需要 MyBatis 复用预处理语句和/或批量更新语句（包括插入和删除）吗？
    
     */
    
    @Test
    public void sqlSessionTest(){
        // openSession() 会开启一个事务， 它不会自动提交
        // 会从当前环境配置的Datasource实例中获取Connection
        // 事务隔离级别使用jdbc或数据源默认的配置
        // 不会复用预处理语句， 每次都是新建一个处理语句
        try(SqlSession sqlSession=this.sqlSessionFactory.openSession();){
            UserMapper mapper=sqlSession.getMapper(UserMapper.class);
            User user=mapper.selectUser(1);
            sqlSession.commit();
            Assert.assertTrue(user.getUserId() == 1);
        }
    }
    
    /*
    SqlSession openSession() //默认开启事务， 不自动提交
    SqlSession openSession(boolean autoCommit)  // 可设置是否自定提交
    SqlSession openSession(Connection connection)  // 需要使用自定义的connection, 传递参数即可， 注意并未覆写同时设置 Connection 和 autoCommit 两者的方法，因为 MyBatis 会使用正在使用中的、设置了 Connection 的环境
    SqlSession openSession(TransactionIsolationLevel level) // 事务隔离级别 
    SqlSession openSession(ExecutorType execType,TransactionIsolationLevel level)
    SqlSession openSession(ExecutorType execType)
    SqlSession openSession(ExecutorType execType, boolean autoCommit)
    SqlSession openSession(ExecutorType execType, Connection connection)
    Configuration getConfiguration();
    
    ExecutorType.SIMPLE：这个执行器类型不做特殊的事情。它为每个语句的执行创建一个新的预处理语句。
    ExecutorType.REUSE：这个执行器类型会复用预处理语句。
    ExecutorType.BATCH：这个执行器会批量执行所有更新语句，如果 SELECT 在它们中间执行，必要时请把它们区分开来以保证行为的易读性。
     */
    @Test
    public void sqlSessionTest2(){
        SqlSession sqlSession=null;
        try{
            /*
            批量执行 
            DEBUG [main] - ==>  Preparing: insert into user(user_id, user_name, age, country) values (?, ?, ?, ?) 
            DEBUG [main] - ==> Parameters: 11(Integer), ttxxxxxx(String), 11(Integer), null
            DEBUG [main] - ==> Parameters: 12(Integer), ttxxxxx12(String), 12(Integer), null
             */
            sqlSession=sqlSessionFactory.openSession(ExecutorType.BATCH);
            UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
            
            
            User user=new User();
            user.setUserId(11);
            user.setAge(11);
            user.setUserName("ttxxxxxx");
            userMapper.insertUser(user);
            
            
            user.setUserId(12);
            user.setAge(12);
            user.setUserName("ttxxxxx12");
            userMapper.insertUser(user);
            
            sqlSession.commit();
            
        }catch (Exception e){
            e.printStackTrace();
            if(sqlSession!=null){
                sqlSession.rollback();
            }
        }finally {
            if(sqlSession!=null){
                sqlSession.close();
            }
        }
    }
    
    
    /*
    sqlSession执行语句的的方法
    注意, statement是mapper对应的类名+方法名， 用于指定对应的sql语句
    
    <T> T selectOne(String statement, Object parameter)
    <E> List<E> selectList(String statement, Object parameter)
    <T> Cursor<T> selectCursor(String statement, Object parameter)  //返回一个游标， 可延迟获取数据
    <K,V> Map<K,V> selectMap(String statement, Object parameter, String mapKey)
    int insert(String statement, Object parameter)
    int update(String statement, Object parameter)
    int delete(String statement, Object parameter)
     */
    @Test
    public void execSqlTest(){
        try(SqlSession sqlSession=this.sqlSessionFactory.openSession()){
            
            Cursor<User> userCursor=sqlSession.selectCursor("com.ttx.example.mapper.UserMapper.selectUser", 1); // statement 是 类名+方法名
            
            Iterator<User> iter=userCursor.iterator();
            while (iter.hasNext()){
                User user=iter.next();
                System.out.println(user.getUserId()+" : "+ user.getUserName());
            }
        }
    }
    
    
    
    
    
    // 本地缓存 (session级别) 
    @Test
    public void cacheTest(){
        /*
        每当一个新 session 被创建，MyBatis 就会创建一个与之相关联的本地缓存。
        任何在 session 执行过的查询语句本身都会被保存在本地缓存中，那么，相同的查询语句和相同的参数所产生的更改就不会二度影响数据库了。
        本地缓存会被增删改、提交事务、关闭事务以及关闭 session 所清空
         */
        try(SqlSession sqlSession=this.sqlSessionFactory.openSession()){
            UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
            User user=userMapper.selectUser(1);
            
//            sqlSession.commit();  // 本地缓存会被增删改、提交事务、关闭事务以及关闭session清空 ， 如果提交事务，将会清空本地缓存，两次查询的结果将不是同一个对象 
            
            User user2=userMapper.selectUser(1);
            
            Assert.assertTrue(user == user2); // 因为使用了本地缓存， 所以只查询了一次， 第二次返回的是同一个对象
            
        }
    }


    // 注解 @SelectProvider
    @Test
    public void sqlProviderTest(){
        
        try(SqlSession sqlSession=this.sqlSessionFactory.openSession()){
            UserSqlProviderMapper mapper=sqlSession.getMapper(UserSqlProviderMapper.class);
            User user=mapper.selectUserById(1);
            
            Assert.assertTrue(user.getUserId()==1);
        }
        
    }
    
    
    
    

    // TODO  分页插件
    
    
    
    
    
    
}
