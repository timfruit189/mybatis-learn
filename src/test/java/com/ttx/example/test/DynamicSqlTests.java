package com.ttx.example.test;

import com.ttx.example.entity.User;
import com.ttx.example.mapper.DynamicSqlUserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * 测试使用动态sql
 *  官方文档http://www.mybatis.org/mybatis-3/zh/dynamic-sql.html
 * @author TimFruit
 * @date 2019/7/4 8:21
 */

public class DynamicSqlTests {

    private SqlSession sqlSession;
    private DynamicSqlUserMapper dynamicSqlUserMapper;

    @Before
    public void init() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        this.sqlSession=sqlSessionFactory.openSession();
        this.dynamicSqlUserMapper=sqlSession.getMapper(DynamicSqlUserMapper.class);
    }
    
    @After
    public void destroy(){
        this.sqlSession.close();
    }
    
    @Test
    public void ifTest(){
        // 测试if动态sql

        User user=dynamicSqlUserMapper.selectUserByUserNameAndAge("tim", null);
        Assert.assertTrue(user!= null);

        user=dynamicSqlUserMapper.selectUserByUserNameAndAge("tim", 999);
        Assert.assertTrue(user == null);
    }
    
    @Test
    public void chooseTest(){
        // 测试choose动态sql
        List<User> users=dynamicSqlUserMapper.selectUserLikeByChoose("t%", 11, null); // 注意， like xx% , 其中%由参数带入控制 
        sqlSession.commit();
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()==1);

        users=dynamicSqlUserMapper.selectUserLikeByChoose("t%", null, "%国"); // 注意， like xx% , 其中%由参数带入控制 
        sqlSession.commit();
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()>1);
        
    }
    
    @Test
    public void whereTest(){
        // 使用where 元素
        List<User> users=dynamicSqlUserMapper.selectUserLikeByWhere("t%", "          "); //country参数中的空格会被过滤， select * from user WHERE user_name like ? 
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()>1);
        
        
        // select * from user WHERE country like ? 
        // 第一个为null, 拼接时， 会去掉 第二个符合条件的 and , 之后再拼接
        users=dynamicSqlUserMapper.selectUserLikeByWhere(null , "中国");
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()==1);


        // select * from user 
        // 没有符合的条件， 不会拼接sql, "where" 关键字不会被拼接  
        users=dynamicSqlUserMapper.selectUserLikeByWhere(null , null);
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()==5);
        
    }


    @Test
    public void trimTest(){
        // 使用trim 元素, 达到和where一样的效果
        List<User> users=dynamicSqlUserMapper.selectUserLikeByTrim("t%", "          "); //country参数中的空格会被过滤， select * from user WHERE user_name like ? 
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()>1);


        // select * from user WHERE country like ? 
        // 第一个为null, 拼接时， 会去掉 第二个符合条件的 and , 之后再拼接
        users=dynamicSqlUserMapper.selectUserLikeByTrim(null , "中国");
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()==1);


        // select * from user 
        // 没有符合的条件， 不会拼接sql, "where" 关键字不会被拼接  
        users=dynamicSqlUserMapper.selectUserLikeByTrim(null , null);
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()==5);

    }
    
    
    @Test
    public void foreachTest(){
        List<Integer> userIds= Arrays.asList(1,2,3);
        
        // select * from user where user_id in ( ? , ? , ? ) 
        List<User> users=dynamicSqlUserMapper.selectUserByUserIdsByForeach(userIds);
        
        Assert.assertTrue(users!=null);
        Assert.assertTrue(users.size()==3);
        
    }
    
    
    
    
}
