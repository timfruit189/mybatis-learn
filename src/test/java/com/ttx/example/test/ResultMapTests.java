package com.ttx.example.test;

import com.ttx.example.mapper.UserResultMapper;
import com.ttx.example.pojo.UserResult;
import com.ttx.example.pojo.UserResultEmail;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 结果映射
 *  官方文档http://www.mybatis.org/mybatis-3/zh/sqlmap-xml.html#Result_Maps
 * @author TimFruit
 * @date 2019/7/3 16:19
 */

public class ResultMapTests {
    private SqlSession sqlSession;
    private UserResultMapper userResultMapper;

    @Before
    public void init() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        this.sqlSession=sqlSessionFactory.openSession();
        this.userResultMapper=sqlSession.getMapper(UserResultMapper.class);
    }

    @After
    public void destroy(){
        this.sqlSession.close();
    }
    
    
    @Test
    public void associationTest(){
        UserResultEmail userResultEmail=userResultMapper.selectUserAndEmailByUserId(1);

        Assert.assertTrue(userResultEmail!=null);

        System.out.println("userName: "+userResultEmail.getUserResult().getUserName());
        System.out.println("emailSize: "+userResultEmail.getUserResult().getUserName());
    }
    
    
    
    
    @Test
    public void autoMappingTest(){
        // 测试结果集下划线和驼峰式的自动映射转换
        List<UserResult> userResults=userResultMapper.selectUserByUserNameLike("t%");
        Assert.assertTrue(userResults.size()>1);
    }
}
