package com.ttx.example.test;

import com.ttx.example.entity.User;
import com.ttx.example.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * 简单的增删查改案例
 * @author TimFruit
 * @date 2019/7/2 17:15
 */

public class MybatisTests {
    private SqlSession sqlSession;
    private UserMapper userMapper;
    
    @Before
    public void init() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        this.sqlSession=sqlSessionFactory.openSession();
        this.userMapper=sqlSession.getMapper(UserMapper.class);
    }

    @After
    public void destroy(){
        this.sqlSession.close();
    }
    
    
    @Test
    public void selectTest(){
        // 查询
        User user=userMapper.selectUser(1);
        sqlSession.commit();
        
        System.out.println("name: "+user.getUserName()+", age: "+user.getAge());

        Assert.assertEquals(new Integer(1), user.getUserId());
    }
    
    // 由于junit运行单个类的所有测试方法是没有顺序的，所以有可能出现主键冲突
    // 可以单个方法运行测试
    @Test
    public void insertTest(){
        // 插入
        User user=createTestUser();
        userMapper.insertUser(user);

        sqlSession.commit();  // 需要提交， 数据库才会有数据
        
        User user_=userMapper.selectUser(9);
        Assert.assertEquals(true, user_!=null);
        Assert.assertEquals(new Integer(9), user_.getUserId());
        System.out.println(user_.getUserName());
    }
    
    
    @Test
    public void updateTest(){
        User user=userMapper.selectUser(9);
        if(user==null){
            user=createTestUser();
            userMapper.insertUser(user);

            sqlSession.commit();  // 需要提交， 数据库才会有数据
        }
        
        //修改
        userMapper.updateUserNameById(9, "update_name");
        sqlSession.commit();
        
        User user_=userMapper.selectUser(9);
        Assert.assertEquals("update_name", user_.getUserName());
    }
    
    @Test
    public void deleteTest(){
        User user=userMapper.selectUser(9);
        if(user==null){
            user=createTestUser();
            userMapper.insertUser(user);

            sqlSession.commit();  // 需要提交， 数据库才会有数据
        }
        
        
        //删除
        userMapper.deleteUserById(9);
        sqlSession.commit();

        User user_=userMapper.selectUser(9);
        Assert.assertEquals(null, user_);
    }
    
    
    
    private User createTestUser(){
        User user=new User();
        user.setUserId(9);
        user.setAge(99);
        user.setUserName("insert_example");
        user.setCountry("china");
        return user;
    }
    
}
