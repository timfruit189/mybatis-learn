package com.ttx.example.test;

import com.ttx.example.entity.User;
import com.ttx.example.entity.UserCache;
import com.ttx.example.mapper.UserCacheMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * 测试使用mybatis的二级缓存机制 
 *  官方文档 http://www.mybatis.org/mybatis-3/zh/sqlmap-xml.html#cache
 *  
 *  本地缓存使用方法可以查看{@link ApiTests#cacheTest()}
 *  
 * @author TimFruit
 * @date 2019/7/5 16:11
 */

public class CacheTests {
    private SqlSessionFactory sqlSessionFactory;


    @Before
    public void initWithXmlConfig() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
    
    @Test
    public void CacheTest(){
        try(SqlSession sqlSession=this.sqlSessionFactory.openSession()){

            UserCacheMapper userCacheMapper=sqlSession.getMapper(UserCacheMapper.class);
            UserCache userCache=userCacheMapper.selectUserCacheByUserId(1);

            sqlSession.commit(); // 提交之后， local cache (本地缓存)失效
            
            // 使用二级缓存
            // 日志： Cache Hit Ratio [com.ttx.example.mapper.UserCacheMapper]: 0.5
            // 从日志可看出命中二级缓存
            UserCache userCache2=userCacheMapper.selectUserCacheByUserId(1);

            //由于是使用序列化拷贝，返回的， 所以不是同一个对象 
            Assert.assertTrue(userCache != userCache2);
            Assert.assertTrue(userCache.getUserId() == userCache2.getUserId());
        }
        
    }


    @Test
    public void CacheTest2(){
        // 需要配置 <cache readOnly="true"/> 才可通过测试  
        try(SqlSession sqlSession=this.sqlSessionFactory.openSession()){

            UserCacheMapper userCacheMapper=sqlSession.getMapper(UserCacheMapper.class);
            User user=userCacheMapper.selectUserByUserId(1);

            sqlSession.commit(); // 提交之后， local cache (本地缓存)失效

            // 使用二级缓存
            // 日志：Cache Hit Ratio [com.ttx.example.mapper.UserCacheMapper]: 0.5
            // 从日志可看出命中二级缓存
            User user2=userCacheMapper.selectUserByUserId(1);
            
            // 由于设置了readOnly=true, 所以返回同一个对象 
            Assert.assertTrue(user==user2);

        }

    }
    
    
    
}
